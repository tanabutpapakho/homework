package com.springboot.homework.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Helloworld {
	@GetMapping("/helloworld")
	public String helloworld() {
		return "Hello world";		
	}

}
